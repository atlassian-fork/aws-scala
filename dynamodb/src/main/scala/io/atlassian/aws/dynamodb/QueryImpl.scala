package io.atlassian.aws
package dynamodb

import com.amazonaws.services.dynamodbv2.model.{ Condition, QueryRequest }
import DynamoDB.{ ReadConsistency, SelectAttributes }
import collection.JavaConverters._

private[dynamodb] object QueryImpl {
  def forHash[K](tableName: String, keyCol: NamedColumn[K])(
    hashKey: K,
    exclusiveStartKey: Option[DynamoMap] = None,
    scanDirection: ScanDirection = ScanDirection.Ascending,
    consistency: ReadConsistency = ReadConsistency.Eventual,
    select: Option[SelectAttributes] = None,
    limit: Option[Int] = None): QueryImpl =
    QueryImpl(
      tableName,
      Map(keyCol.name -> condition(hashKey, Comparison.Eq)(keyCol.column)),
      exclusiveStartKey,
      scanDirection,
      consistency,
      select,
      limit
    )

  def forHashAndRange[K, R](tableName: String, keyCol: NamedColumn[K], ordCol: NamedColumn[R])(
    hashKey: K,
    rangeKey: R,
    rangeComparison: Comparison,
    exclusiveStartKey: Option[DynamoMap] = None,
    scanDirection: ScanDirection = ScanDirection.Ascending,
    consistency: ReadConsistency = ReadConsistency.Eventual,
    select: Option[SelectAttributes] = None,
    limit: Option[Int] = None): QueryImpl =
    QueryImpl(
      tableName,
      Map(
        keyCol.name -> condition(hashKey, Comparison.Eq)(keyCol.column),
        ordCol.name -> condition(rangeKey, rangeComparison)(ordCol.column)
      ),
      exclusiveStartKey,
      scanDirection,
      consistency,
      select,
      limit
    )

  private def condition[K](key: K, comparator: Comparison)(kc: Column[K]) =
    new Condition().withComparisonOperator(Comparison.asAWS(comparator)).withAttributeValueList(
      kc.marshall.toFlattenedMap(key).values.asJavaCollection)
}

private[dynamodb] case class QueryImpl(table: String,
                                       keyConditions: Map[String, Condition],
                                       exclusiveStartKey: Option[DynamoMap],
                                       scanDirection: ScanDirection,
                                       consistency: ReadConsistency,
                                       select: Option[SelectAttributes],
                                       limit: Option[Int]) {
  def asQueryRequest: QueryRequest = {
    val req =
      new QueryRequest()
        .withTableName(table)
        .withKeyConditions(keyConditions.asJava)
        .withScanIndexForward(ScanDirection.asBool(scanDirection))
        .withConsistentRead(ReadConsistency.asBool(consistency))
    limit.foreach { req.setLimit(_) }
    exclusiveStartKey.foreach { esk => req.setExclusiveStartKey(esk.asJava) }
    select.foreach(s => req.withSelect(SelectAttributes.asSelect(s)))
    req
  }
}
